import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.*;

public class LAYOUTCity {

    JFrame f;
    private int[][] text;
    private Color bColour[][][] = new Color[20][20][2];
    private int airport;
    private int condo;
    private int university;
    private int school;
    private int hospital;
    private int airportDimension;
    private int condoDimension;
    private int schoolDimension;
    private int hospitalDimension;
    private int universityDimension;
    
            

    public LAYOUTCity(int hospital, int school, int airport, int condo, int university) {
        f = new JFrame("City (MAP 1)");
        this.hospital=hospital;
        this.school = school;
        this.condo = condo;
        this.airport= airport;
        this.university= university;
        this.airportDimension=airportDimension;
        this.schoolDimension=schoolDimension;
        this.hospitalDimension=hospitalDimension;
        this.condoDimension=condoDimension;
        this.universityDimension=universityDimension;
    }

    public LAYOUTCity() {
    }
    public int AirportDimension(){
        int airportDimension= airport/2;
        return airportDimension;
    }
    public int CondoDimension(){
        if(condo>=5){
        int condoDimension= condo/4;
        return condoDimension;
        }
        else if(condo<=5){
        int condoDimension= condo/2;
        return condoDimension;
        }
        return condoDimension;
    }
    public int HospitalDimension(){
        int hospitalDimension= hospital/4;
        return hospitalDimension;
    }
    public int SchoolDimension(){
        int schoolDimension= school/2;
        return schoolDimension;
    }
    public int UniversityDimension(){
        int universityDimension= university/8;
        return universityDimension;
    }
    public void JButtonPrinting() {
         JButton[][] button = new JButton[20][20];
         String[][] text=new String[20][20];
        //printing out for airport;
        
        for(int i=4; i< AirportDimension()+4; i++ ){
            for( int j=18 ; j<20; j++){
                text[i][j]="A";
                bColour[i][j][0]=Color.blue;
                bColour[i][j][1]=Color.white;
                
            }
        }
        
        //printing out for condominiums
        if(CondoDimension()>=4){
        for(int i = 9; i< CondoDimension()+9;i++){
            for( int j=12; j<16; j++){
                text[i][j]="C";
                bColour[i][j][0]=Color.RED;
                bColour[i][j][1]=Color.WHITE;
                
            }
        }
        for(int i =0; i< CondoDimension()+0;i++){
            for( int j=6; j<10; j++){
                text[i][j]="C";
                bColour[i][j][0]=Color.RED;
                bColour[i][j][1]=Color.WHITE;
            }
        }
        for(int i =15; i< CondoDimension()+15;i++){
            for( int j=0; j<4; j++){
                text[i][j]="C";
                bColour[i][j][0]=Color.RED;
                bColour[i][j][1]=Color.WHITE;
            }
        }
        }
        
        if(CondoDimension()<=4){
        for(int i = 9; i< 11;i++){
            for( int j=12; j<CondoDimension()+12; j++){
                text[i][j]="C";
                bColour[i][j][0]=Color.RED;
                bColour[i][j][1]=Color.WHITE;
            }
        }
        for(int i =0; i< 2;i++){
            for( int j=6; j<CondoDimension()+6; j++){
                text[i][j]="C";
                bColour[i][j][0]=Color.RED;
                bColour[i][j][1]=Color.WHITE;
            }
        }
        for(int i =15; i< 17;i++){
            for( int j=0; j<CondoDimension(); j++){
                text[i][j]="C";
                bColour[i][j][0]=Color.RED;
                bColour[i][j][1]=Color.WHITE;
            }
        }
        }
        
        //printing out hospitals
        for(int i =9; i< HospitalDimension()+9;i++){
            for( int j=7; j<11; j++){
                text[i][j]="H";
                bColour[i][j][0]=Color.GRAY;
                bColour[i][j][1]=Color.BLACK;
            }
        }
        //printing out schools
         for(int i =0; i< SchoolDimension()+0;i++){
            for( int j=2; j<4; j++){
                text[i][j]="S";
                bColour[i][j][0]=Color.DARK_GRAY;
                bColour[i][j][1]=Color.WHITE;

            }
        }
         //prniting out universities
         for(int i =17; i< UniversityDimension()+17;i++){
            for( int j=6; j<14; j++){
                text[i][j]="U";
                bColour[i][j][0]=Color.ORANGE;
                bColour[i][j][1]=Color.BLACK;
            }
        }
         
        // For the bus
        for( int i =8; i< 12; i++){
            for( int j=3; j< 6 ; j++){
                text[i][j]="BS";
                bColour[i][j][0]=Color.LIGHT_GRAY;
                bColour[i][j][1]=Color.BLACK;
            }
        }
        //for the subway centre
        for( int i =0 ;i< 4;i++){
            for( int j=12; j< 16;j++){
                text[i][j]="SC";
                bColour[i][j][0]=Color.PINK;
                bColour[i][j][1]=Color.BLACK;
            }
        }
        // for the road 
        for ( int i = 2; i< 15; i++){
            for( int j = 17; j< 18; j++){
                text[i][j]="^";
            }
        }
        for( int i =0; i< 3; i++){
            for( int j =17 ; j < 18; j++){
                text[i][j]="^<";
            }
        }
        for( int i =14; i< 15; i++){
            for( int j =5; j< 16; j++){
                text[i][j]="<";
            }
        }
        for( int i = 15 ; i< 16; i++){
            for ( int j =8; j< 17; j++){
                text[i][j]=">";
            }
        }
        for( int i = 1; i< 4; i++){
            for ( int j =19; j < 20; j++){
                text[i][j]="^";
            }
        }
        for( int i = 2; i < 4; i++){
            for( int j =18; j< 19; j++){
                text[i][j]="v";
            }
        }
        for( int i =1; i< 2; i++){
            for( int j = 18; j<19; j++){
                text[i][j]=">v";
             } 
        }
        for(int i =6; i<7; i++){
            for( int j=6 ; j< 16; j++){
                text[i][j]="< >";
            }
        }
        for ( int i =7; i< 15; i++){
            for( int j= 1 ; j< 2; j++){
                text[i][j]="v";
            }
        }
        for( int i=7; i<15 ; i++){
            for ( int j=0; j< 1; j++){
                text[i][j]="^";
            }
        }
        for( int i =1; i< 6; i++){
            for( int j= 0; j< 1; j++){
                text[i][j]="^";
            }
        }
         for( int i =2; i< 6; i++){
            for( int j= 1; j< 2; j++){
                text[i][j]="v";
            }
        }
         for( int i =6; i< 7; i++){
            for( int j= 0; j< 2; j++){
                text[i][j]="< >^v";
            }
        }
         for( int i =4; i< 6; i++){
            for( int j= 12; j< 14; j++){
                text[i][j]="^";
            }
        }
          for( int i =4; i< 6; i++){
            for( int j= 12; j< 13; j++){
                text[i][j]="v";
            }
        }
          for( int i =7; i< 9; i++){
            for( int j= 13; j< 14; j++){
                text[i][j]="v";
            }
        }
          for( int i =4; i< 6; i++){
            for( int j= 13; j< 14; j++){
                text[i][j]="^";
            }
        }
          for( int i =7; i< 9; i++){
            for( int j= 12; j< 13; j++){
                text[i][j]="^";
            }
        }
          for( int i =1; i< 6; i++){
            for( int j= 4; j< 5; j++){
                text[i][j]="v";
            }
        }
          for( int i =2; i< 6; i++){
            for( int j= 5; j< 6; j++){
                text[i][j]="^";
            }
        }
           for( int i =7; i< 8; i++){
            for( int j= 3; j< 4; j++){
                text[i][j]="^";
            }
        }
           for( int i =7; i< 8; i++){
            for( int j= 4; j< 5; j++){
                text[i][j]="v";
            }
        }
            for( int i =0; i< 1; i++){
            for( int j= 0; j< 1; j++){
                text[i][j]="<v";
            }
        }
        for( int i =0; i< 1; i++){
            for( int j= 1; j< 2; j++){
                text[i][j]="<";
            }
        } 
        for( int i =6; i< 7; i++){
            for( int j= 2; j< 3; j++){
                text[i][j]="< >";
            }
        }    
        for( int i =6; i< 7; i++){
            for( int j= 3; j< 4; j++){
                text[i][j]="< >^";
            }
        }   
        for( int i =6; i< 7; i++){
            for( int j= 4; j< 5; j++){
                text[i][j]="< >v";
            }
        }   
        for( int i =6; i< 7; i++){
            for( int j= 5; j< 6; j++){
                text[i][j]="< >^";
            }
        }
        for( int i =0; i< 1; i++){
            for( int j= 4; j< 5; j++){
                text[i][j]="<v";
            }
        }  
        for( int i =0; i< 1; i++){
            for( int j= 5; j< 6; j++){
                text[i][j]="<";
            }
        }  
        for( int i =1; i< 2; i++){
            for( int j= 5; j< 6; j++){
                text[i][j]="^>";
            }
        }  
        for( int i =15; i< 20; i++){
            for( int j= 4; j< 5; j++){
                text[i][j]="v";
            }
        }
        for( int i =16; i< 20; i++){
            for( int j= 5; j< 6; j++){
                text[i][j]="^";
            }
        }  
        for( int i =14; i< 15; i++){
            for( int j= 16; j< 17; j++){
                text[i][j]="v<";
            }
        }  
        for( int i =15; i< 16; i++){
            for( int j= 17; j< 18; j++){
                text[i][j]=">^";
            }
        }  
        for( int i =14; i< 15; i++){
            for( int j= 4; j< 5; j++){
                text[i][j]="v<";
            }
        } 
         for( int i =15; i< 16; i++){
            for( int j= 5; j< 6; j++){
                text[i][j]="^>";
            }
        }
         for( int i =15; i< 16; i++){
            for( int j= 6; j< 7; j++){
                text[i][j]=">^";
            }
        }  
         for( int i =15; i< 16; i++){
            for( int j= 7; j< 8; j++){
                text[i][j]=">v";
            }
        }  
         for( int i =1; i< 2; i++){
            for( int j= 1; j< 2; j++){
                text[i][j]=">";
            }
        }  
         for( int i =0; i< 1; i++){
            for( int j= 18; j< 19; j++){
                text[i][j]="<";
            }
        }  
         for( int i =7; i< 14; i++){
            for( int j= 16; j< 17; j++){
                text[i][j]="v";
            }
        }  
         for( int i =0; i< 6; i++){
            for( int j= 16; j< 17; j++){
                text[i][j]="v";
            }
        }  
         for( int i =6; i< 7; i++){
            for( int j= 16; j< 17; j++){
                text[i][j]="< >v";
            }
        }  
          for( int i =0; i< 1; i++){
            for( int j= 19; j< 20; j++){
                text[i][j]="<^";
            }
        }  
          for( int i =7; i< 9; i++){
            for( int j= 7; j< 8; j++){
                text[i][j]="v";
            }
        }  
          for( int i =7; i< 9; i++){
            for( int j= 8; j< 9; j++){
                text[i][j]="^";
            }
        }  
          for( int i =16; i< 17; i++){
            for( int j= 6; j< 7; j++){
                text[i][j]="^";
            }
        }  
          for( int i =16; i< 17; i++){
            for( int j= 7; j< 8; j++){
                text[i][j]="v";
            }
        }  
          for( int i =6; i< 7; i++){
            for( int j= 12; j< 14; j++){
                text[i][j]="< >v^";
            }
        }  
          for( int i =6; i< 7; i++){
            for( int j= 7; j< 8; j++){
                text[i][j]="< >^";
            }
        }  
           for( int i =6; i< 7; i++){
            for( int j= 8; j< 9; j++){
                text[i][j]="< >v";
            }
        }  
          
         


         
        
        //For loop to assign value into Jbutton and then to print it out
            
            for (int i = 0; i < 20; i++) {
                for (int j = 0; j < 20; j++) {
                    button[i][j]= new JButton(text[i][j]);
                    f.add(button[i][j]);
                    button[i][j].setBackground(bColour[i][j][0]);
                    button[i][j].setForeground(bColour[i][j][1]);
                }
            }
            f.setLayout(new GridLayout(20, 20));
            f.setSize(200, 200);
            f.setVisible(true);

        JButton b401 = new JButton("NEXT");
        f.add(b401);
        ActionListener sub = new Subroutine();
        b401.addActionListener(sub);
        f.setLayout(new GridLayout(21, 20));
        
        //the button for subway layout 
        
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.dispose();
        f.setSize(1200, 600);
        f.setVisible(true);
        
    }
    private class Subroutine implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.out.println("Button pressed"+e.getActionCommand());
            f.dispose();
            SUBWAYCity m=new SUBWAYCity();
            m.JButttonSubwayPrinting();
    }
    
    
                    
        }
    }
            

        
       
