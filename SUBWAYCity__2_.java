
package pkgnew.assignment;
import java.awt.*;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.util.Arrays;
import javax.swing.*;

public class SUBWAYCity {
    JFrame j;
    Color[][][] b2Colour = new Color[20][20][2];

    
    public SUBWAYCity(){
        j= new JFrame("Urban Planning (MAP 2)");
        

    }
    public void JButttonSubwayPrinting(){
        JButton [][] buttonSubway= new JButton[20][20];
        String [][] textSubway= new String[20][20];
        //print subway  station
        for( int i =0 ;i< 4;i++){
            for( int k=12; k< 16;k++){
                textSubway[i][k]="SC";
                b2Colour[i][k][0]= Color.PINK;
                b2Colour[i][k][1]= Color.black;
                
                
            }
        }
        //printing subway line
        for( int i =0;i < 1; i++){
            for ( int k=1; k< 12; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =1;i < 10; i++){
            for ( int k=1; k< 2; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =9;i < 18; i++){
            for ( int k=0; k< 1; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =17;i < 18; i++){
            for ( int k=1; k< 4; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =10;i < 17; i++){
            for ( int k=3; k< 4; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =10;i < 18; i++){
            for ( int k=5; k< 6; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =17;i < 18; i++){
            for ( int k=6; k<8 ; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =9;i < 18; i++){
            for ( int k=7; k<8 ; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =9;i < 10; i++){
            for ( int k=8; k<19 ; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =1;i < 9; i++){
            for ( int k=18; k<19 ; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =1;i < 2; i++){
            for ( int k=16; k<18 ; k++){
                textSubway[i][k]="#";
            }
        }
        for( int i =10; i< 11; i++){
            for( int k=4; k< 5; k++){
                textSubway[i][k]="#";

            }
        }
         for( int i =4; i< 5; i++){
            for( int k=18; k< 19; k++){
                textSubway[i][k]="#";
                b2Colour[i][k][0]= Color.PINK;
                b2Colour[i][k][1]= Color.black;
            }
        }
        for (int i = 9; i < 10; i++) {
            for (int k = 12; k < 13; k++) {
                textSubway[i][k] = "#";
                b2Colour[i][k][0] = Color.PINK;
                b2Colour[i][k][1] = Color.black;
            }
        }
        
          for (int i = 9; i < 10; i++) {
            for (int k = 7; k < 8; k++) {
                textSubway[i][k] = "#";
                b2Colour[i][k][0] = Color.PINK;
                b2Colour[i][k][1] = Color.black;
            }
        }
          
           for (int i = 0; i < 1; i++) {
            for (int k = 6; k < 7; k++) {
                textSubway[i][k] = "#";
                b2Colour[i][k][0] = Color.PINK;
                b2Colour[i][k][1] = Color.black;
            }
        }
        for (int i = 15; i < 16; i++) {
            for (int k = 0; k < 1; k++) {
                textSubway[i][k] = "#";
                b2Colour[i][k][0] = Color.PINK;
                b2Colour[i][k][1] = Color.black;
            }
        }
        for (int i = 17; i < 18; i++) {
            for (int k = 6; k < 7; k++) {
                textSubway[i][k] = "#";
                b2Colour[i][k][0] = Color.PINK;
                b2Colour[i][k][1] = Color.black;
            }
        }
        for (int i = 11; i < 12; i++) {
            for (int k = 3; k < 4; k++) {
                textSubway[i][k] = "#";
                b2Colour[i][k][0] = Color.PINK;
                b2Colour[i][k][1] = Color.black;
            }
        }
        for (int i = 0; i < 1; i++) {
            for (int k = 2; k < 3; k++) {
                textSubway[i][k] = "#";
                b2Colour[i][k][0] = Color.PINK;
                b2Colour[i][k][1] = Color.black;
            }
        }
              



//        for (int i=0; i<20; i++){
//            for (int k=0; k<20; k++){
//                if ((textSubway[i][k]).equals("#")){
//                    b2Colour[i][k][0]=Color.black;
//                    b2Colour[i][k][1]=Color.white;
//                }
//                else if ((textSubway[i][k]).equals("SC")){
//                    b2Colour[i][k][0]=Color.pink;
//                    b2Colour[i][k][1]=Color.black;
//                }
//                else{
//                    b2Colour[i][k][0]=Color.LIGHT_GRAY;
//                    b2Colour[i][k][1]=Color.black;
//                }
//                        
//            }
//        }
        
        for(int i=0; i< 20; i++){
            for( int k=0 ; k< 20;k++){
                buttonSubway[i][k]=new JButton(textSubway[i][k]);
                j.add(buttonSubway[i][k]);
                buttonSubway[i][k].setBackground(b2Colour[i][k][0]);
                buttonSubway[i][k].setForeground(b2Colour[i][k][1]);
            }
        }
        
        j.setLayout(new GridLayout(20, 20));
        j.setSize(200, 200);
        j.setVisible(true);
    }
    
    
}
